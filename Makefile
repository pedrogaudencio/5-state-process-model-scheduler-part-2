all: compilar run

# compila o codigo
compilar:
	javac src/*.java -d classes

# corre o codigo e apresenta output como pedido
run:
	CLASSPATH=classes:. java Main teste.txt

# corre o codigo e apresenta output detalhado
verbose-output:
	CLASSPATH=classes:. java Main teste.txt
	less output.txt

# apaga os ficheiros .class
clean:
	rm -rf classes/*.class output.txt