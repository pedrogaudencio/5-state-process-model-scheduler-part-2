import java.util.*;

public class New extends Estado{
	
	public New(){
		super.queue = new LinkedList<Processo>();
	}

	public String toString(){
		String output="";
		Iterator<Processo> it = queue.iterator();
		if(it.hasNext()){
			while(it.hasNext()){
				Processo p = it.next();
				if(p!=null)
					output += "Processo {"+p.toString()+"}\n";
				else
					output += "tem null\n";
			}
		}
		else
			output += "vazio\n";
		return output;
	}
}