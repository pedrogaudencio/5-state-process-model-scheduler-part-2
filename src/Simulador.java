import java.util.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Simulador{

	//estados
	New novo = new New();
	Ready ready = new Ready();
	Running running = new Running();
	Blocked blocked = new Blocked(1, 3);
	Exit exit = new Exit();



	int mem_size = 300;
	int mem_manager = 1; // 1 - best-fit, 2 - worst-fit
	Memory memory = new Memory(mem_size, mem_manager);

	//input
	int numProcessos = 0, clock;
	ArrayList<String> listaProcessos = new ArrayList<String>();

	FileOutputStream fop = null;
	File file;
	static String output = "";
	int output_type = 1; // 1 - normal, 2 - debug


	public Simulador(ArrayList<String> listainput){
		//cria lista de processos
		while(!listainput.isEmpty()){
			String linha2=listainput.remove(0);
			String[] tokens2 = linha2.split(" ");
			String primeiro=tokens2[0];
			
			tokens2[0]="";
			String resto="";
			//System.out.println(primeiro);
			for(String a: tokens2)
				resto=resto+a;
			
			//System.out.println("Linha: " +resto+"\n");
			listaProcessos.add(primeiro);
			listaProcessos.add(resto);
			numProcessos++;
		}
		//System.out.println("numProcessos: "+numProcessos);
		/*
		for(int j=0;j<listaProcessos.size();j++){
			j++;
			System.out.println(listaProcessos.get(j));
			int i = listaProcessos.get(j).length()+10;
			System.out.println("listaProcessos.get("+j+").length()+10: "+i);
		}*/
	}

	// metodo principal para ciclar o simulador
	public void scheduler(){
		clock=0;
		while(exit.getQueue().size()!=numProcessos){

			if(!listaProcessos.isEmpty() && !memory.takes(listaProcessos.get(1).length()+10)){
				String instr;
				for(int i=0;i<listaProcessos.size();i++){
					instr = Integer.toString(Integer.parseInt(listaProcessos.get(i))+1);
					//System.out.println("i: "+i+", instr: "+instr);
					listaProcessos.set(i,instr);
					i++;
				}
				//System.out.println("stall");
			}
			else{
				if(!listaProcessos.isEmpty() && Integer.parseInt(listaProcessos.get(0))==clock){
					listaProcessos.remove(0);
					Processo p = memory.malloc(listaProcessos.remove(0));
					if(output_type == 1){
						System.out.println("admit():");
						for(int i : p.getRefs())
							System.out.print(memory.getInstr(i)+" ");
						System.out.print("\n");
					}
					novo.enqueue(p);
				}
			}
			//System.out.println(memory.toString());

			if(output_type == 2 && clock<10)
				System.out.println(memory);

			//printStatus(clock);
			collectOutput();

			event_occurs();

			// run/*
			if(running.isEmpty() || running.isNull()){
				/*if(!ready.isNull())
					ready.dequeue();
				else{*/
					if(!ready.isEmpty()){
						if(ready.getQueue().peek().getFlag()){
							dispatch(); // saca do ready e mete no running
							for(Processo p : ready.getProcessos())
								p.setFlag(true);
						}
						else{
							for(Processo p : ready.getProcessos())
								p.setFlag(true);
						}
					}
				//}
			}
			else{
				if(!running.isEmpty() && !running.isNull()){
					int instrucao = memory.getInstr(running.next().getInstrPos());
					//System.out.println("instrucao: "+instrucao);
					int variavel = memory.getInstr(running.next().getInstrPos()+1);
					//System.out.println("variavel: "+variavel);

					/*
					for(Processo p : running.getQueue())
						System.out.println(p);
					*/

					switch (instrucao) {
						case 0:
							//System.out.println("Zero");
							memory.setVar(running.next().getVarByPos(variavel), 0);
							break;
						case 1:
							//System.out.println("Var++: "+memory.getInstr(running.next().getVarByPos(variavel)));
							memory.setVar(running.next().getVarByPos(variavel), memory.getInstr(running.next().getVarByPos(variavel))+1);
							break;
						case 2:
							//System.out.println("Var--: "+memory.getInstr(running.next().getVarByPos(variavel)));
							memory.setVar(running.next().getVarByPos(variavel), memory.getInstr(running.next().getVarByPos(variavel))-1);
							break;
						case 3:
							if(memory.getInstr(running.next().getVarByPos(variavel))==0){}
								//System.out.println("if 0=="+memory.getInstr(running.next().getVarByPos(variavel))+", pc+1");
							else{
								//System.out.println("if 0!="+memory.getInstr(running.next().getVarByPos(variavel))+", pc+2");
								running.next().pcForw();
								running.next().pcForw();
							}
							break;
						case 4: //go to instr back
							int basdf = running.next().getVarByPos(variavel)*2-2;
							//System.out.println("set pc-="+basdf);
							running.next().setPC(running.next().getPC()-variavel*2-2);
							break;
						case 5: //go to instr forward
							int basdfq = running.next().getVarByPos(variavel)*2-2;
							//System.out.println("set pc+="+basdfq);
							running.next().setPC(running.next().getPC()+variavel*2-2);
							break;
						case 6: //processo p.fork
							//System.out.println("Fork");
							break;
						case 7:
							//System.out.println("--> Block");
							event_wait();
							break;
						case 8:
							//System.out.println("Copy");
							memory.setVar(running.next().getVarByPos(0), memory.getInstr(running.next().getVarByPos(variavel)));
							break;
						case 9:
							//System.out.println("\n\n-----\nclock: "+clock);
							//System.out.println("Releasing");
							release();
							break;
					}
				}
				if(running.isZero()){
					timeout(); // saca do running e mete no ready
					running.resetTimeout();
				}
			}
			
			admit();
			tictac();
		}

		clean_memory();

		//printStatus(clock);

		collectOutput();
		writeToFile();
	}

	// retira o processo da fila new e coloca na fila ready
	public void admit(){
		if(!ready.isFull() && novo.getQueue().peek()!=null){
			ready.enqueue(novo.dequeue());
			output += "vai fazer admit()\n";
		}
	}

	// retira o processo da fila ready e coloca na fila running
	public void dispatch(){
		running.enqueue(ready.dequeue());
		output += "vai fazer dispatch()\n";
	}

	// retira o processo da fila running e coloca na fila ready
	public void timeout(){
		if(!running.isNull()){
			ready.enqueue(running.dequeue());
			output += "vai fazer timeout()\n";
		}
	}

	// retira o processo da fila running e coloca na fila exit
	public void release(){
		if(output_type == 1){
			System.out.println("release():");
			for(int p : running.next().getRefs())
				System.out.print(memory.getInstr(p)+" ");
			System.out.print("\n");
		}
		memory.mfree(running.next());
		exit.enqueue(running.dequeue());
		output += "vai fazer release()\n";
	}

	// retira o processo da fila running e coloca na fila blocked (no dispositivo de destino)
	public void event_wait(){
		blocked.enqueue(running.dequeue());
		output += "vai fazer event_wait()\n";
	}

	// retira o processo da fila blocked (do dispositivo respetivo) e coloca na fila ready
	public void event_occurs(){
		Iterator<Dispositivo> it = blocked.getDispositivos().iterator();
		while(it.hasNext()){
			Dispositivo d = it.next();
			if(d.getContador()==0){ // se o contador interno do dispositivo for zero, saca do dispositivo e mete no ready
				output += "vai fazer event_occurs() - enqueue do processo do dispositivo "+d.getID()+" na fila ready\n";
				d.peekQueue().setFlag(false);
				ready.enqueue(d.dequeue());
			}
		}
	}

	// metodo para incrementação e decrementação temporal de todo o scheduler
	public void tictac(){
		// processo no running
		if(!running.isNull()){
			running.decrementaTimeout();
			//running.
			for(Processo p : running.getQueue()){
				p.pcForw();
				p.pcForw();
				//System.out.println(p.toString());
			}
		}
		// clock do dispositivo
		for(Dispositivo d : blocked.getDispositivos())
			d.clock();
		clock++;
	}

	public void clean_memory(){
		if(exit.getQueue().size()==numProcessos){
			memory.clear();
		}
	}

	// imprime output na consola
	public void printStatus(int t){
		String s = "< "+t+" > \t";
		for(Processo p : novo.getQueue())
			s += "P(pc="+p.getPC()+")-[new]>  ";

		for(Processo p : ready.getQueue())
			s += "P(pc="+p.getPC()+")-[ready]>  ";

		for(Dispositivo d : blocked.getDispositivos())
			for(Processo p : d.getProcessos())
				s += "P(pc="+p.getPC()+")-[blocked]>  ";

		for(Processo p : running.getQueue())
			s += "P(pc="+p.getPC()+")-[running]>  ";

		for(Processo p : exit.getQueue())
			s += "P(pc="+p.getPC()+")-[exit]> ";

		System.out.println(s);
	}
	

	// metodo verboso para output.txt
	public void collectOutput(){
		output += "\n*************************************************************************************\nclock="+
							clock+"\n*************************************************************************************\n\n";
		output += "-------------------------\nnew:\n"+novo.toString()+"\n";
		output += "-------------------------\nready:\n"+ready.toString()+"\n";
		output += "-------------------------\nblocked:\n"+blocked.toString()+"\n";
		output += "-------------------------\nrunning:\n"+running.toString()+"\n";
		output += "-------------------------\nexit:\n"+exit.toString()+"\n";
	}

	// escreve output detalhado de cada ciclo num ficheiro output.txt
	public void writeToFile(){
		try{
		file = new File("output.txt");
		fop = new FileOutputStream(file);

		if(!file.exists())
			file.createNewFile();

			byte[] content = output.getBytes();
 
			fop.write(content);
			fop.flush();
			fop.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if (fop != null) {
					fop.close();
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}