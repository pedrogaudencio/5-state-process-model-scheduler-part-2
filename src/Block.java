import java.util.*;

public class Block{

	private int begin, end, size;

	public Block(int b, int e){
		begin = b;
		end = e;
		if(b!=0)
			size = end-begin;
		else
			size = end-begin+1;
	}

	public int getBegin(){
		return begin;
	}

	public int getEnd(){
		return end;
	}

	public int getSize(){
		return size;
	}

	public void setBegin(int b){
		begin = b;
		size = end-begin;
	}

	public void setEnd(int e){
		end = e;
		size = end-begin;
	}

	public String toString(){
		String s = "[" + begin + "-" + end + "]";
		return s;
	}

}