import java.util.*;

public class Processo{

    private int pc;
    private int refs[];
    private String estado;
    private String estados[]={"New", "Ready", "Running", "Blocked", "Exit"};
    private boolean flag;

    public Processo(int m_refs[]){
        refs = m_refs;
        pc = 8;
        flag = true;
        estado = estados[0];
    }

    public boolean isProcessed(){
        System.out.println("refs.length: " + refs.length);
        return pc==refs.length;
    }

    public void setRefs(int m_refs[]){
        refs = m_refs;
    }

    public void setRef(int pos, int ref){
        refs[pos]=ref;
    }

    public void pcForw(){
        pc++;
    }

    public int getPC(){
        return pc;
    }

    public void setPC(int pc){
        this.pc=pc;
    }

    public int[] getRefs(){
        return refs;
    }

    public int getRefBegin(){
        return refs[0];
    }

    public int getRefEnd(){
        return refs[refs.length-1];
    }

    public int getInstrPos(){
        //System.out.println("getInstrPos: refs["+pc+"]="+refs[pc]);
        return refs[pc];
    }

    public int getVarByPos(int i){
        //System.out.println("getVarByPos: refs["+i+"]="+refs[i]);
        return refs[i];
    }

    public void setEstado(int e){
        estado = estados[e-1];
    }

    public String getEstado(){
        return estado;
    }

    // retorna flag
    public boolean getFlag(){
        return flag;
    }

    // altera a flag
    public void setFlag(boolean f){
        flag = f;
    }

    public String toString(){
        String output = "";
        for(int p=0;p<refs.length;p++)
            output += refs[p] + " ";
        output += "\n";
        return output;
    }
}
