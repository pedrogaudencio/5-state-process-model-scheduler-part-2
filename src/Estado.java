import java.util.Queue;
import java.util.LinkedList;

public abstract class Estado{
	Queue<Processo> queue;

	public Queue<Processo> getQueue(){
		return queue;
	}

	// retorna true se o elemento da cabeça da fila running é null
	public boolean isNull(){
		return queue.peek()==null;
	}

	// coloca o processo p na fila do respetivo estado
	public void enqueue(Processo p){
		queue.offer(p);
		Simulador.output += "vai fazer enqueue do "+p+"\n";
	}

	// retira e devolve o processo que está na cabeça da fila do respetivo estado
	public Processo dequeue(){
		Simulador.output += "vai fazer dequeue do "+queue.peek()+"\n";
		return queue.poll();
	}
}