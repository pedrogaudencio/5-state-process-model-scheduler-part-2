import java.util.*;
import java.io.*;
import java.lang.*;

public class Main{
	
	public static void main(String[] args){
		//ler do input
		ArrayList<String> listainput  = new ArrayList<String>();
		try {
			FileReader fr = new FileReader(args[0]);
			BufferedReader br = new BufferedReader(fr);
			String s = br.readLine();

			while(s!=null){
				listainput.add(s);
				//System.out.println("Linha: " + s);
				s = br.readLine();
			}

			fr.close();

		}
		catch(Exception e) {
			System.out.println("Exception: " + e);
		}

		Simulador s1 = new Simulador(listainput);
		s1.scheduler();

	}
}