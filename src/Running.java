import java.util.*;

public class Running extends Estado{

	private int timeout=4, contadortimeout;

	public Running(){
		contadortimeout=timeout;
		super.queue = new LinkedList<Processo>();
	}

	// retorna o timeout
	public int getTimeout(){
		return timeout;
	}

	// retorna o contador de timeout
	public int getContador(){
		return contadortimeout;
	}

	// decrementa 1 no contador de timeout
	public void decrementaTimeout(){
		if(!queue.isEmpty() && queue.peek()!=null){
			contadortimeout--;
		}
		else
			if(contadortimeout!=timeout){
				contadortimeout=timeout;
				Simulador.output += "contador timeout vai igualar ao timeout\n";
			}
		
	}

	// volta a colocar o timeout inicialmente definido
	public void resetTimeout(){
		contadortimeout=timeout;
	}

	// retorna true se o contador de timeout já chegou a zero
	public boolean isZero(){
		return contadortimeout==0;
	}

	// retorna true se a fila running está vazia
	public boolean isEmpty(){
		//System.out.println("running is empty? "+queue.isEmpty()+", tem "+queue.peek());
		return queue.isEmpty();
	}

	// retorna o processo que está na cabeça da fila running
	public Processo next(){
		return queue.peek();
	}

	public String toString(){
		String output="";
		Iterator<Processo> it = queue.iterator();
		if(it.hasNext()){
			while(it.hasNext() && queue.peek()!=null){
				Processo p = it.next();
				if(p!=null)
					output += "Processo {"+p.toString()+"}\n";
				else
					output += "tem null\n";
			}
		}
		else
			output += "vazio\n";
		return output;
	}
}