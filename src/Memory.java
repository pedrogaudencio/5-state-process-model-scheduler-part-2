import java.util.*;

public class Memory{
	private int[] mem;
	private int occupied, mem_manager, limite;
	private LinkedList<Block> empty;

	public Memory(int m){
		mem=new int[300];
		occupied=0;
		mem_manager=m;
		limite=300;
		empty=new LinkedList<Block>();
		empty.add(new Block(0, 299));
	}

	public Memory(int l, int m){
		mem=new int[l];
		occupied=0;
		mem_manager=m;
		limite=l;
		empty=new LinkedList<Block>();
		empty.add(new Block(0, l-1));
	}

	public void setLimite(int l){
		int new_mem[] = new int[l];
		for(int i : mem)
		new_mem[i] = mem[i];
		mem = new_mem;
		limite=l;
	}

	public boolean takes(int n){
		boolean b = limite-occupied>=n;
		//System.out.println("takes "+n+"? "+b+" (occupied: "+occupied+" / free: "+(limite-occupied)+")");
		return limite-occupied>=n;
	}

	public int getOccupied(){
		return occupied;
	}

	public int getInstr(int p){
		//System.out.println("memory.getInstr("+p+")="+mem[p]);
		return mem[p];
	}

	public void setVar(int p, int var){
		mem[p]=var;
	}

	public Processo malloc(String input){
		if(mem_manager==1)
			return malocBest(input);
		else
			return malocWorst(input);
	}

	public Processo malocBest(String input){
		int block_index=0, size_needed=input.length()+10;
		occupied+=size_needed;
		Block block=empty.peek();

		if(empty.size()>1){
			while(block_index<empty.size()){
				if(empty.get(block_index).getSize()>empty.get(block_index+1).getSize() && empty.get(block_index).getSize()>=size_needed){
					block=empty.get(block_index);
					//System.out.println("block bestfit: "+block.toString());
					break;
				}
				block_index++;
			}
			empty.remove(block_index);
		}
		else
			block=empty.poll();
		block_index=0;

		if(block.getSize()>size_needed){
			Block new_block = new Block(block.getBegin()+size_needed,block.getEnd());
			block = new Block(block.getBegin(),block.getBegin()+size_needed-1);
			//System.out.println("block usado: "+block.toString());

			if(empty.isEmpty()){
				//System.out.println("empty vazio: "+block.toString());
				empty.add(new_block);
			}
			else{
				while(block_index<empty.size()){
					//System.out.println("block atual: "+empty.get(block_index));
					if(block_index+1>empty.size()){
						//System.out.println("block no fim: "+empty.get(block_index));
						empty.add(new_block);
					}
					else{
						if(new_block.getSize()<empty.get(block_index).getSize()){
							//System.out.println("block no sitio: "+empty.get(block_index));
							empty.add(block_index, new_block);
							break;
						}
					}
					block_index++;
				}
			}
		}
		reparticionateBlocks();

		int instrs[] = new int[size_needed];
		int j=0;

		// aloca variaveis i<block.getBegin()+10
		int i=block.getBegin();
		while(j<10){
			//System.out.println("instrs["+j+"]: "+i);
			instrs[j]=i;
			i++;
			j++;
		}

		/*
		System.out.println("size_needed: "+size_needed);
		System.out.println("begin: "+block.getBegin());
		System.out.println("end: "+block.getEnd());
		System.out.println("size: "+block.getSize());
		System.out.println("empty: "+empty.toString());
		*/

		j=0;
		// aloca instruções
		for(i=block.getBegin()+10;i<=block.getEnd();i++){
			mem[i]=(int) input.charAt(j) - 48;
			//System.out.println("mem["+i+"]: "+input.charAt(j) + " = " + mem[i]);
			int ja=j+10;
			//System.out.println("instrs["+ja+"]: "+i);
			instrs[j+10]=i;
			j++;
		}

		Processo p = new Processo(instrs);
		//System.out.println(p.toString());
		return p;
	}

	public Processo malocWorst(String input){
		int block_index=0, size_needed=input.length()+10;
		occupied+=size_needed;

		Block block=empty.poll();

		if(block.getSize()>size_needed){
			Block new_block = new Block(block.getBegin()+size_needed,block.getEnd());
			block = new Block(block.getBegin(),block.getBegin()+size_needed-1);
			//System.out.println("block usado: "+block.toString());

			if(empty.isEmpty()){
				//System.out.println("empty vazio: "+block.toString());
				empty.add(new_block);
			}
			else{
				while(block_index<empty.size()){
					//System.out.println("block atual: "+empty.get(block_index));
					if(block_index+1>empty.size()){
						//System.out.println("block no fim: "+empty.get(block_index));
						empty.add(new_block);
					}
					else{
						if(new_block.getSize()<empty.get(block_index).getSize()){
							//System.out.println("block no sitio: "+empty.get(block_index));
							empty.add(block_index, new_block);
							break;
						}
					}
					block_index++;
				}
			}
		}
		reparticionateBlocks();

		int instrs[] = new int[size_needed];
		int j=0;

		// aloca variaveis i<block.getBegin()+10
		int i=block.getBegin();
		while(j<10){
			//System.out.println("instrs["+j+"]: "+i);
			instrs[j]=i;
			i++;
			j++;
		}

		/*
		System.out.println("size_needed: "+size_needed);
		System.out.println("begin: "+block.getBegin());
		System.out.println("end: "+block.getEnd());
		System.out.println("size: "+block.getSize());
		System.out.println("empty: "+empty.toString());
		*/

		j=0;
		// aloca instruções
		for(i=block.getBegin()+10;i<=block.getEnd();i++){
			mem[i]=(int) input.charAt(j) - 48;
			//System.out.println("mem["+i+"]: "+input.charAt(j) + " = " + mem[i]);
			int ja=j+10;
			//System.out.println("instrs["+ja+"]: "+i);
			instrs[j+10]=i;
			j++;
		}

		Processo p = new Processo(instrs);
		//System.out.println(p.toString());
		return p;
	}

	public void mfree(Processo processo){
		Block new_block = new Block(processo.getRefBegin(),processo.getRefEnd());
		//occupied-=new_block.getSize();
		int current = 0;
		for(Block b : empty){
			//System.out.println("block b: "+b.toString());
			if(new_block.getSize()>empty.get(current).getSize() || current+1 == empty.size()){
				//System.out.println("new_block: "+new_block.toString());
				empty.add(current, new_block);
				break;
			}
			current++;
		}
		//System.out.println("occupied (mfree): "+occupied);
		reparticionateBlocks();
	}

	public void reparticionateBlocks(){
		int current = 0;
		while(current+1<empty.size()){
			if(empty.get(current).getBegin() == empty.get(current+1).getEnd()+1){
				empty.get(current).setBegin(empty.get(current+1).getBegin());
				empty.remove(current+1);
			}
			if(empty.get(current).getEnd() == empty.get(current+1).getBegin()-1){
				empty.get(current).setEnd(empty.get(current+1).getEnd());
				empty.remove(current+1);
			}
			//System.out.println("reparticionateBlocks: "+empty.get(current).toString());
			current++;
		}
		occupied=limite-empty.peek().getSize();
		//System.out.println("occupied (reparticionateBlocks): "+occupied);
		//System.out.println("empty: "+empty);
	}

	public void clear(){
		mem=new int[limite];
		while(!empty.isEmpty())
			empty.remove();
		empty.add(new Block(0, limite-1));
	}

	public String toString(){
        String output = "";
        for(int p=0;p<mem.length;p++){
        	if(p%30==0)
            	output += "\n" + mem[p] + " ";
            else
            	output += mem[p] + " ";
        }
        output += "\n";
        return output;
    }
}