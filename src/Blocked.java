import java.util.*;

public class Blocked extends Estado{

	private int numDispositivos;
	private ArrayList<Dispositivo> dispositivos = new ArrayList<Dispositivo>();

	public Blocked(int n, int t){
		numDispositivos=n;
		dispositivos.add(new Dispositivo(1,t));
	}

	// retorna a lista de dispositivos
	public ArrayList<Dispositivo> getDispositivos(){
		return dispositivos;
	}

	// retorna o dispositivo com o id do index
	public Dispositivo getDispositivo(int index){
		return dispositivos.get(index);
	}

	// coloca na fila do dispositivo de destino [p.getDestino()-1] o processo p
	public void enqueue(Processo p){
		//System.out.println("*** destino do processo: "+p.getResto().get(0));
		Simulador.output += "vai fazer enqueue do "+p+" no dispositivo\n";
		dispositivos.get(0).enqueue(p);
	}

	public String toString(){
		return new String("Dispositivo {"+dispositivos.get(0).toString()+"}\n");
	}
}